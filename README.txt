CCK Multigroup Tabs
=====

CCK Multigroups Tabs allows to display CCK multigroups into tabs.
https://drupal.org/sandbox/manumilou/2177731

Features
========

- Provide multigroup support to CCK Fieldgroup Tabs.
- Allows using one of the fieldgroup field as the tab title.

Requirements
============

- CCK
  http://drupal.org/project/cck
- Content Multigroup
  http://drupal.org/project/cck
- CCK Fieldgroup Tabs
  http://drupal.org/project/cck_fieldgroup_tabs
- The following patch: https://drupal.org/files/issues/cck_fieldgroup_tabs_multigroup_support_v7.patch
- Drupal 6.x
  http://drupal.org/project/drupal
- PHP safe mode is not supported, depending on your Feeds Importer configuration
  safe mode may cause no problems though.
- PHP 5.2.x recommended

Installation
============

- Install CCK Multigroup Fields
- Apply the following patch to CCK Fieldgroup Tabs: https://drupal.org/files/issues/cck_fieldgroup_tabs_multigroup_support_v7.patch
- A new tab CCK Fieldgroup Tabs has appeared on every content type edit form. The feature can be enabled and configured by content type.
- Create a Multigroup fieldgroup in the content type
- Create a content and add instances of the multigroup field. They now should be displayed as tab on the node view page.


